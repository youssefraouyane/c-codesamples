#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <chrono>

using namespace std::chrono;

// The condition variable
std::condition_variable data_cv;
std::condition_variable completed_cv;

std::mutex data_mutex;
std::mutex completed_mutex;

// shared data
std::string status;

// bool flag for predicate
bool condition_data = false;
bool condition_download = false;


void progressBar()
{
	while (true)
	{
		std::this_thread::sleep_for(2s);
		std::unique_lock<std::mutex> uniq_lck(data_mutex);

		// Lambda predicate that checks the flag
		data_cv.wait(uniq_lck, [] {return condition_data; });
		std::cout << "Progress is now: " << status << " \n";
		condition_data = false;
	
		uniq_lck.unlock();

		std::unique_lock<std::mutex> completed_lock(completed_mutex);
		if (completed_cv.wait_for(completed_lock, 10ms, [] { return condition_download > 0; }))
		{
			std::cout << "Download progress: " << status << "%\r";
			return;
		}
	}
}


void fetching()
{
	std::cout << "Fetching thread locking mutex\n";
	std::cout << "Fetching thread has locked the mutex\n";

	// Call wait()
	// This will unlock the mutex and make this thread
	// sleep until the condition variable wakes us up
	std::cout << "Fetching thread sleeping...\n";

	// Lambda predicate that checks the flag
	std::this_thread::sleep_for(2s);
	for (int i = 0; i <= 10; ++i)
	{
		std::this_thread::sleep_for(2s);
		std::unique_lock<std::mutex> uniq_lck(data_mutex);
		std::this_thread::sleep_for(200ms);
		const int currentIndex = i * 10;
		status =  "Receiving " + std::to_string(currentIndex) + " data.\n";
		condition_data = true;
		uniq_lck.unlock();
		data_cv.notify_all();
	}

	std::lock_guard lk(completed_mutex);
	completed_cv.notify_all();
	condition_download = true;
}



void processing()
{
	std::unique_lock<std::mutex> unique_lock(completed_mutex);
	std::cout << "Processing thread sleeping...\n";

	completed_cv.wait(unique_lock, []() {return condition_download; });

	std::lock_guard<std::mutex> data_lck(data_mutex);
	std::cout << "Processing ..."<<std::endl;
	std::this_thread::sleep_for(2s);
	std::cout << "Finished Processing." << std::endl;
}

int main()
{
	std::thread fetcher{ fetching };
	std::thread processor{ processing };
	std::thread progressThread{ progressBar };


	fetcher.join();
	processor.join();
	progressThread.join();
}
