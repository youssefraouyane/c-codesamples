#include <iostream>

// Simple implementation of loop without using neither the for nor the while loop

void weirdForLoop(int start, int end, int increment)
{
	int n = start;

ForLoopBlock:
	if (n < end)
	{
		std::cout << "Number = " << n << " .\n";
		n = n + increment;
		goto ForLoopBlock;
	}
}

int main()
{
	weirdForLoop(0, 15, 1);


	return 0;
}